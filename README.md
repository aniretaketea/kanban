This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

### `npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

### `npm run build`


## Concession made

### Safe from style overrides
No encapsulation tools are used and "simple" class names are used as selectors (which isn't feasible for real projects)

## Known issues

### Most component update on every state change
E.g., the whole app lights up during typing in textarea
