import React from 'react';
import './App.scss';
import Board from './Board';
import ControlPanel from './ControlPanel';
import TaskView from './TaskView';
import Lightbox from './common/Lightbox';

import { connect } from 'react-redux';

import { 
  setActiveTaskId, 
  setTaskCreation,
  setEditedTaskInfo } from '../redux/actions/actions';

const App = ({ 
  activeTaskId, 
  isCreationInProgress, 
  setActiveTaskId, 
  setTaskCreation,
  setEditedTaskInfo }) => {
  
  const showTaskLightbox = activeTaskId !== null || isCreationInProgress;
  
  const closeTaskLightbox = () => {
      
    setEditedTaskInfo('', '');

    isCreationInProgress ?
      setTaskCreation(false) :
      setActiveTaskId(null);
  };
  
  return (
    
    <div 
      className="kanban-app">
      
      <Board 
        className="kanban-app__board"/>
      
      <ControlPanel
        onAddNewTaskCLick={() => setTaskCreation(true)}
        className="kanban-app__controls"/>
      
      {
        showTaskLightbox ?
          
          <Lightbox
            onScreenClick={closeTaskLightbox}
            onCloseCLick={closeTaskLightbox}>
            
            <TaskView />
          </Lightbox>
          
          : null
      }
    </div>
  );
};

const mapStateToProps = state => {
  
  const { activeTaskId, taskCreation: isCreationInProgress } = state;
  
  return { activeTaskId, isCreationInProgress };
};

export default connect(
  mapStateToProps, 
  { 
    setActiveTaskId, 
    setTaskCreation, 
    setEditedTaskInfo 
  }
)(App);
