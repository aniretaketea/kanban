import React from 'react';
import _ from 'lodash';
import './Board.scss';
import Stage from './Stage';
import TaskCard from './TaskCard';

import { connect } from 'react-redux';
import { getStagesWithTasks } from '../redux/selectors/selectors';
import { 
  startTaskEditing, 
  moveTaskBack, 
  moveTaskForward } from '../redux/actions/actions';

const Board = ({ 
  stagesWithTasks, 
  startTaskEditing, 
  moveTaskBack,
  moveTaskForward,
  className }) => {
  
  return (
    
    <div
      className={className + " board"}>
      
      <div 
        className="board__wrapper">

        {
          _.values(stagesWithTasks).map( stage => {

            return (

              <Stage
                key={ stage.id }
                title={ stage.title }
                className="board__stage">
                {
                  _.values(stage.tasks).map( task => {
                    return (

                      <TaskCard
                        key={ task.id }
                        id={ task.id }
                        title={ task.title }
                        onTaskClick={() => startTaskEditing(
                          task.id,
                          task.title,
                          task.description
                        )}
                        onMoveNextClick={() => moveTaskForward(task.id)}
                        onMovePreviousClick={() => moveTaskBack(task.id)}>
                      </TaskCard>
                    );
                  })
                }
              </Stage>
            );
          })
        }
      </div>
    </div>
  );
};

const mapStateToProps = state => {

  return {
    stagesWithTasks: getStagesWithTasks(state)
  };
};

export default connect(
  mapStateToProps, 
  { 
    startTaskEditing, 
    moveTaskBack, 
    moveTaskForward 
  }
)(Board);
