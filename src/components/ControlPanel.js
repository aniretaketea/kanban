import React from 'react';
import './ControlPanel.scss';
import Button from './common/Button';

const ControlPanel = ({ onAddNewTaskCLick, className }) => {
  
  return (
    
    <div
      className={className + " control-panel"}>
      
      <Button
        onClick={onAddNewTaskCLick}
        className="control-panel__button">
        Add new task
      </Button>
    </div>
  );
};

export default ControlPanel;
