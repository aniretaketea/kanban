import React from 'react';
import './Stage.scss';

const Stage = ({ title, children, className }) => {

  const tasks = Array.isArray(children) ?
    children :
    [ children ];
  
  return (
    
    <div 
      className={className + " stage"}>
      
      <div 
        className="stage__header">

        <h2
          className="stage__title">
          { title }
        </h2>
      </div>
      
      <ul
        className="stage__tasks">
        {
          tasks.map( (task) => {
            return (
              <li
                key={task.props.id}
                className="stage__task">
                { task }
              </li>
            );
          })
        }
      </ul>
    </div>
  );
};

export default Stage;
