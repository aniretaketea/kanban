import React from 'react';
import './TaskCard.scss';
import TaskStageControls from './TaskStageControls';

const TaskCard = ({ 
  title, 
  onTaskClick, 
  onMoveNextClick, 
  onMovePreviousClick }) => {
  
  const handleMoveNextClick = (event) => {
    
    event.stopPropagation();
    onMoveNextClick(event);
  };

  const handleMovePreviousClick = (event) => {

    event.stopPropagation();
    onMovePreviousClick(event);
  };

  return (

    <div
      onClick={onTaskClick}
      className="task-card">

      { title }
      
      <TaskStageControls
        onMoveNextClick={handleMoveNextClick}
        onMovePreviousClick={handleMovePreviousClick} 
        className="task-card__stage-controls" />
    </div>
  );
};

export default TaskCard;
