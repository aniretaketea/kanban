import React from 'react';
import './TaskStageControls.scss';

const TaskStageControls = ({
  onMoveNextClick,
  onMovePreviousClick,
  className }) => {

  return (

    <div
      className={className + " task-stage-controls"}>

      <button
        onClick={onMovePreviousClick}
        className="task-stage-controls__button task-stage-controls__button--previous">
        {" < "}
      </button>
      
      <button
        onClick={onMoveNextClick}
        className="task-stage-controls__button task-stage-controls__button--next">
        {" > "}
      </button>
    </div>
  );
};

export default TaskStageControls;
