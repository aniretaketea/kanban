import React, { useEffect } from 'react';
import './TaskView.scss';
import TaskStageControls from './TaskStageControls';
import Button from './common/Button';
import { getStageTitleByTaskId } from '../redux/selectors/selectors';

import _ from 'lodash';

import { connect } from 'react-redux';
import { 
  setEditedTaskInfo, 
  editTaskInfo, 
  setActiveTaskId, 
  setTaskCreation,
  createTask,
  moveTaskForward,
  moveTaskBack } from '../redux/actions/actions';

const TaskView = ({ 
  task,
  taskStageTitle,
  isTaskCreation,
  fieldValues, 
  setEditedTaskInfo, 
  editTaskInfo, 
  setActiveTaskId,
  setTaskCreation,
  createTask,
  moveTaskForward,
  moveTaskBack }) => {

  const handleFieldChange = (event) => {
    
    const editData = {
      ...fieldValues,
      [event.target.name]: event.target.value 
    };

    setEditedTaskInfo(..._.values(editData));
  };
  
  const validateTaskFields = taskFieldValues => {
    
    // the only current condition is that the title is present
    return !!taskFieldValues.title;
  };
  
  const handleTaskCreation = (event) => {
    
    if ( validateTaskFields(fieldValues) ) {

      createTask(fieldValues.title, fieldValues.description);
      setEditedTaskInfo('', '');
      setTaskCreation(false);
    }
  };
  
  const handleTaskEdit = (event) => {

    if ( validateTaskFields(fieldValues) ) {

      editTaskInfo(task.id, fieldValues.title, fieldValues.description);
      setEditedTaskInfo('', '');
      setActiveTaskId(null);
    }
  };
  
  const handleFormSubmit = (event) => {
    
    event.preventDefault();

    isTaskCreation ?
      handleTaskCreation(event) :
      handleTaskEdit(event);
  };
  
  const titleFieldRef = React.createRef();
  
  useEffect(() => {
    
    titleFieldRef.current.focus();
  }, []);
  
  return (
    
    <div
      className="task-view">

      {
        isTaskCreation ?
          
        null :

        <div
          className="task-view__task-stage">

          <div 
            className="task-view__task-stage-title">
            { taskStageTitle }
          </div>
          
          <TaskStageControls
            onMoveNextClick={() => moveTaskForward(task.id)}
            onMovePreviousClick={() => moveTaskBack(task.id)}
            className="task-view__task-stage-controls"/>
        </div>
      }
      
      <form 
        onSubmit={handleFormSubmit}
        className="task-view__form">
        
        <textarea
          name="title"
          value={fieldValues.title}
          onChange={handleFieldChange}
          ref={titleFieldRef}
          placeholder={"Task"}
          className="task-view__field task-view__title">
        </textarea>

        <textarea
          name="description"
          value={fieldValues.description}
          onChange={handleFieldChange}
          className="task-view__field task-view__description">
        </textarea>

        <Button
          className="task-view__save-button">
          Save
        </Button>
      </form>
    </div>
  );
};

const mapStateToProps = state => {
  
  const taskStage = state.taskCreation ?
    '' :
    getStageTitleByTaskId(state, state.activeTaskId);
  
  return {
    task: state.tasks[state.activeTaskId],
    taskStageTitle: taskStage, 
    isTaskCreation: state.taskCreation,
    fieldValues: {
      title: state.editedTaskInfo.title,
      description: state.editedTaskInfo.description
    }
  }
};

export default connect(
  mapStateToProps, 
  { 
    setEditedTaskInfo, 
    editTaskInfo, 
    setActiveTaskId, 
    setTaskCreation, 
    createTask,
    moveTaskForward,
    moveTaskBack 
  }
)(TaskView);
