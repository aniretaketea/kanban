import React from 'react';
import './Lightbox.scss';

const Lightbox = ({ 
  onCloseCLick, 
  onScreenClick, 
  children }) => {
  
  return (

    <div
      className="lightbox">
      
      <div 
        onClick={onScreenClick}
        className="lightbox__screen">
      </div>

      <div
        className="lightbox__container">

        <button
          onClick={onCloseCLick}
          className="lightbox__close-button">
          x
        </button>

        { children }
      </div>
    </div>
  );
};

export default Lightbox;
