import React from 'react';
import ReactDOM from 'react-dom';
import './common/normalize.css';
import './common/reset.css';
import './index.scss';

import { Provider } from 'react-redux';
import store from './redux/store';

import App from './components/App';

ReactDOM.render(
  <Provider
    store={store}>
    
    <App />
  </Provider>, 
  document.getElementById('root')
);
