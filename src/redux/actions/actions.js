import { 
  SET_ACTIVE_TASK_ID,
  SET_EDITED_TASK_INFO,
  START_TASK_EDIT,
  EDIT_TASK_INFO,
  SET_TASK_CREATION,
  CREATE_TASK,
  MOVE_TASK_FORWARD,
  MOVE_TASK_BACK
} from './actionTypes';

export const setActiveTaskId = taskId => {
  
  return {
    type: SET_ACTIVE_TASK_ID,
    payload: {
      taskId
    }
  };
};

export const setEditedTaskInfo = (title, description) => {

  return {
    type: SET_EDITED_TASK_INFO,
    payload: {
      title,
      description
    }
  };
};

export const startTaskEditing = (taskId, title, description) => {
  
  return {
    type: START_TASK_EDIT,
    payload: {
      taskId,
      title,
      description
    }
  };
};

export const editTaskInfo = (taskId, title, description) => {
  
  return {
    type: EDIT_TASK_INFO,
    payload: {
      taskId,
      title,
      description
    }
  };
};

export const setTaskCreation = (status) => {
  
  return {
    type: SET_TASK_CREATION,
    payload: status
  };
};

export const createTask = (title, description) => {
  
  return {
    type: CREATE_TASK,
    payload: {
      title,
      description
    }
  };
};

export const moveTaskForward = (taskId) => {
  
  return {
    type: MOVE_TASK_FORWARD,
    payload: {
      taskId
    }
  };
};

export const moveTaskBack = (taskId) => {

  return {
    type: MOVE_TASK_BACK,
    payload: {
      taskId
    }
  };
};
