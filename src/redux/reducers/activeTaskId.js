import {
  SET_ACTIVE_TASK_ID, 
  START_TASK_EDIT
} from '../actions/actionTypes';

export default (state = null, action) => {

  switch (action.type) {

    case SET_ACTIVE_TASK_ID:
      return action.payload.taskId;
      
    case START_TASK_EDIT:
      return action.payload.taskId;
    
    default:
      return state;
  }
};
