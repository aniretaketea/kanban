import {
  SET_EDITED_TASK_INFO, 
  START_TASK_EDIT
} from '../actions/actionTypes';

export default (state = {title: '', description: ''}, action) => {
  
  switch (action.type) {
    
    case SET_EDITED_TASK_INFO:

      const { title, description } = action.payload;
      
      return {
        title,
        description
      };
      
    case START_TASK_EDIT:
      
      return {
        title: action.payload.title,
        description: action.payload.description
      };
      
    default:
      return state;
  }
}
