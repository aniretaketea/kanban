import { combineReducers } from 'redux';
import taskReducer from './tasks';
import stageReducer from './stages';
import activeTaskIdReducer from './activeTaskId';
import editedTaskInfoReducer from './editedTaskInfo';
import taskCreationReducer from './setTaskCreation';

export default combineReducers({
  tasks: taskReducer,
  stages: stageReducer,
  activeTaskId: activeTaskIdReducer,
  editedTaskInfo: editedTaskInfoReducer,
  taskCreation: taskCreationReducer
});
