import { SET_TASK_CREATION } from '../actions/actionTypes';

export default (state = false, action) => {
  
  switch (action.type) {
    
    case SET_TASK_CREATION:
      return action.payload;
      
    default:
      return state;   
  }
}
