import { tasks, 
  getNewTaskId, 
  getStartStageId,
  getNextStageId,
  getPreviousStageId } from '../../static-data'; 
import { 
  EDIT_TASK_INFO, 
  CREATE_TASK,
  MOVE_TASK_FORWARD,
  MOVE_TASK_BACK } from '../actions/actionTypes';

const editTask = (state, payload) => {

  const { taskId, title, description } = payload;

  return {
    ...state,
    [taskId]: {
      ...state[taskId],
      title,
      description
    }
  };
};

const createTask = (state, payload) => {

  const id = getNewTaskId();
  const stageId = getStartStageId();
  const { title, description } = payload;

  return {
    ...state,
    [id]: {
      id,
      title,
      description,
      stageId
    }
  };
};

const moveTask = (state, taskId, getDestinationStageId) => {

  const stageId = getDestinationStageId(state[taskId].stageId);

  return {
    ...state,
    [taskId]: {
      ...state[taskId],
      stageId
    }
  };
};

const moveTaskForward = (state, payload) => {
  
  return moveTask(state, payload.taskId, getNextStageId);
};

const moveTaskBack = (state, payload) => {

  return moveTask(state, payload.taskId, getPreviousStageId);
};

export default ( state = tasks, action ) => {
  
  switch (action.type) {
    
    case EDIT_TASK_INFO:
      
      return editTask(state, action.payload);
      
    case CREATE_TASK:
      
      return createTask(state, action.payload);
      
    case MOVE_TASK_FORWARD:

      return moveTaskForward(state, action.payload);

    case MOVE_TASK_BACK:

      return moveTaskBack(state, action.payload);
      
    default:
      return state;
  }
}
