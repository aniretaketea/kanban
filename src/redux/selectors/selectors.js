import _ from 'lodash';

const organizeTasksByStage = tasks => {

  let tasksByStage = {};
  
  _.forOwn(tasks, task => {
    
    const { stageId, id: taskId } = task;
    
    tasksByStage[stageId] = {
      ...tasksByStage[stageId],
      [taskId]: task
    };
  });
  
  return tasksByStage;
};

const addTasksToStages = (stages, tasks) => {
  
  const tasksByStage = organizeTasksByStage(tasks);
  
  return _.mapValues(stages, stage => (
    {...stage, tasks: tasksByStage[stage.id]}
  ));
};

export const getStagesWithTasks = store => {

  const { stages, tasks } = store;
  
  return addTasksToStages(stages, tasks);
};

export const getStageTitleByTaskId = (store, taskId) => {
  
  return store.stages[store.tasks[taskId].stageId].title;
};
