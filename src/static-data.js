import shortid from 'shortid';
import { sentence, paragraph } from 'txtgen';
import _ from 'lodash';

// util

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// stage data

const generateStages = (titleList) => {
  
  let stages = {},
      stageOrder = [];
  
  titleList.map(title => {
    
    const id = shortid.generate();
    
    stages[id] = {
      id,
      title
    };
    
    stageOrder.push(id);
  });
  
  return [stages, stageOrder];
};

export const [ stages, stageOrder ] = generateStages([
  'To Do', 
  'In Progress',
  'In Review',
  'Done'
]);

export const getStartStageId = () => {

  return stageOrder[0];
};

export const getNextStageId = (currentStageId) => {
  
  const currentIndex = stageOrder.indexOf(currentStageId);
  const currentIsLast = currentIndex === stageOrder.length - 1;

  return currentIsLast ?
    currentStageId :
    stageOrder[currentIndex + 1];
};

export const getPreviousStageId = (currentStageId) => {

  const currentIndex = stageOrder.indexOf(currentStageId);
  const currentIsFirst = currentIndex === 0;
  
  return currentIsFirst ?
    currentStageId :
    stageOrder[currentIndex - 1];
};

// task data

export const getNewTaskId = () => {
  
  return shortid.generate();
};

const generateTask = (stageId) => {
  
  return {
    id: getNewTaskId(),
    title: sentence(),
    description: paragraph(),
    stageId: stageId
  }
};

const generateNumberOfTasksAtStage = (number, stageId) => {

  return Array.from(
    { length: number },
    () => generateTask(stageId)
  );
};

const getTasksForStages = (stages) => {
  
  let tasks = {};
  
  _.forOwn(stages, stage => {

    generateNumberOfTasksAtStage(getRandomInt(0, 6), stage.id)
      .forEach(task => tasks[task.id] = task);
  });
  
  return tasks;
};

export const tasks = getTasksForStages(stages);
